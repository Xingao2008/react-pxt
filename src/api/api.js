import Server from './server';

class API extends Server{
  /**
   *  @method post
   *  @return {promise}
   */
  async uploadImg(params = {}){
    try{
      let result = await this.axios('post', '', params); 
      if(result && result.status === 1){
        return result;
      }else{
        let err = {
          tip: 'fail to upload',
          response: result,
          data: params,
          url: '',
        }
        throw err;
      }
    }catch(err){
      throw err;
    }
  }

  /**
   *  @method get
   *  @return {promise}
   */
  async getRecord(params = {}){
    try{
      let result = await this.axios('get', `/shopro/data/record/${params.type}`); 
      if(result && (result.data instanceof Object) && result.http_code === 200){
        return result.data;
      }else{
        let err = {
          tip: 'fail to get data',
          response: result,
          data: params,
          url: '',
        }
        throw err;
      }
    }catch(err){
      throw err;
    }
  }

  /**
   *  @method get
   *  @return {promise}
   */
  async getProduction(params = {}){
    try{
      let result = await this.axios('get', '/shopro/data/products', params); 
      if(result && (result.data instanceof Object) && result.http_code === 200){
        return result.data.data||[];
      }else{
        let err = {
          tip: 'fail to get product data',
          response: result,
          data: params,
          url: '',
        }
        throw err;
      }
    }catch(err){
      throw err;
    }
  }

  /**
   *  @return {promise}
   */
  async getBalance(params = {}){
    try{
      let result = await this.axios('get', '/shopro/data/balance', params); 
      if(result && (result.data instanceof Object) && result.http_code === 200){
        return result.data.data||{};
      }else{
        let err = {
          tip: 'fail to get fees',
          response: result,
          data: params,
          url: '',
        }
        throw err;
      }
    }catch(err){
      throw err;
    }
  }
}

export default new API();